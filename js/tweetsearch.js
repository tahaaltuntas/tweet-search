$(document).ready(function() {
    var refreshId;
    var interval = 10 * 1000; // 10 seconds

    var countSpinner = $('#count-spinner').spinner({
        spin: function( event, ui ) {
            if ( ui.value < 0 ) {
                $( this ).spinner( "value", 0 );
                return false;
            } 
        }
    });
    var timeSpinner= $('#time-spinner').spinner({
        spin: function( event, ui ) {
            if ( ui.value < 0 ) {
                $( this ).spinner( "value", 0 );
                return false;
            } 
        }
    });
    $("#search-button").click(function() {
        var rpp;
        if (countSpinner.spinner("value") > 0) 
            rpp = countSpinner.spinner("value");
        else {
            rpp = 5;
            countSpinner.spinner("value", 5);
        }
        var resultsType = $('#results-type').val();
        if (timeSpinner.spinner("value") > 0)
            interval = timeSpinner.spinner("value") * 1000;
        else {
            interval = 10 * 1000;
            timeSpinner.spinner("value", 10);
        }
        var query = $("#q").val();
        query = $.trim(query);
        searchTweet(query, rpp, resultsType);
        clearInterval(refreshId);
        refreshId = setInterval(function() { searchTweet(query, rpp, resultsType); }, interval);
    });

    countSpinner.spinner();
    function searchTweet(query, rpp, resultsType) {
        $.ajax({
            dataType: "jsonp",
            url: "http://search.twitter.com/search.json",
            data: {
                q: query,
                rpp: rpp,
                results_type: resultsType
            },
            success: function(data) {
                showResults(data.results);
            }
        });
    }

    function showResults(results) {
        $('#tweets').empty();
        $.each(results, function(index, tweet) {
            $('#tweets').append(createTweet(tweet));
        });
    }

    function createTweet(tweet) {
        var tweetEl = $('<div>');
        var profileLink = 'http://twitter.com/' + tweet.from_user;
        tweetEl.attr('class', 'tweet');
        
        var profilePic = $('<img>');
        profilePic.attr('class', 'tweet-profile-pic');
        profilePic.attr('src', tweet.profile_image_url);
        profilePic = linkify(profileLink, profilePic);
        
        var tweetContent = $('<div>');
        tweetContent.attr('class', 'tweet-content');
        var userName = $('<b>');
        userName.text($.trim(tweet.from_user_name));
        tweetContent.append(linkify(profileLink, userName));
        tweetContent.append(' ' + tweet.from_user, ' (' + tweet.created_at + ')', '<br />');
        var tweetText = $('<p>');
        tweetText.append(tweet.text);
        tweetText.tweetify();
        tweetContent.append(tweetText);
        tweetEl.append(profilePic, tweetContent);
        return tweetEl;
    }

    function linkify(href, inner) {
        var el = $('<a>');
        el.attr('href', href);
        el.append(inner);
        return el;
    }
});
